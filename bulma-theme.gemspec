# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "bulma-theme"
  spec.version       = "0.1.0"
  spec.authors       = ["Rob Dyke"]
  spec.email         = ["robdyke@gmail.com"]

  spec.summary       = "Bulma theme for Jekyll"
  spec.homepage      = "https://gitlab.com/robdyke/bulma-theme#readme"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.2"
end
